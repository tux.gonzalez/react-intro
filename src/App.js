/* eslint-disable jsx-a11y/img-redundant-alt */
import CreateTodoButton from './components/CreateTodoButton';
import TodoCounter from './components/TodoCounter';
import TodoItem from './components/TodoItem';
import TodoList from './components/TodoList';
import TodoSearch from './components/TodoSearch';
import ImageCustom from './images/image.svg';

const todos = [
  {
    text: 'Cortar cebolla',
    completed: false
  },
  {
    text: 'Tomar agua',
    completed: true
  },
  {
    text: 'Aprender Reactjs',
    completed: false
  }
]


function App() {
  return (
    <div className="container mx-auto">
      <div className='flex flex-row space-x-6 h-screen'>
        <div className='w-1/3'>
          <div className='bg-white rounded-lg p-5 my-24'>
            <h2 className='text-4xl font-bold'>Create a new tasks</h2>
            <div className='my-5'>
              <TodoSearch label='Task name' placeholder="Launch rocket to the moon" />
            </div>
            <CreateTodoButton text="Create" />
            <img src={ImageCustom} alt='image' className='mt-5 w-10/12 mx-auto' />
          </div>
        </div>
        <div className='w-2/3'>
          <div className='py-20'>
            <h3 className='text-4xl font-bold text-white text-center'>Your Tasks</h3>
            <div>
              <TodoCounter total={10} completed={2} />
              <TodoList>
                {todos.map((todo, index) => <TodoItem key={index} text={todo.text} completed={todo.completed} />)}
              </TodoList>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
