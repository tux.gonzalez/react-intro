import React from 'react';

const TodoSearch = ({label, placeholder}) => {
  return (
    <div>
      <label className="block mb-5">{label}</label>
      <input
        placeholder={placeholder}
        className="block rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 w-full px-2"
      />
    </div>
  );
}

export default TodoSearch;
