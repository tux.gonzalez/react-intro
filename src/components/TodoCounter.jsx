import React from 'react';

const TodoCounter = ({total, completed}) => {
  return (
    <h1 className="text-xl font-bold underline text-white text-center">
      Completed {completed} of {total}
    </h1>
  );
}

export default TodoCounter;
