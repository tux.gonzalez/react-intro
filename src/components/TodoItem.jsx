import React from 'react';

const TodoItem = ({text, completed}) => {
  return (
    <li>
      <span>{completed ? 'true' : 'false'}</span>
      <p>{ text }</p>
      <span>X</span>
    </li>
  );
}

export default TodoItem;
